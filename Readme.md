# Pelican Latex Prerender

> Plugin that converts all your latex equations to HTML *during compile-time*. Once compiled,
> your HTML is free of any javascript concerning the latex. Just html and css. Thanks to [KaTex](https://katex.org/).
> 
> For an article that uses the prerenderer, check out my post on [Least Squares Meshes](http://thousandyardstare.de/blog/what-are-least-squares-meshes.html)

## Requirements

- Requires you to have `node` installed and available in your `PATH`, such that `katex_convert.py` can find it

## Limitations

- Only works with markdown (e.g. no restructured text)
- Only parses display latex (`$$...$$`) if it has its own paragraph

## Installing

1. Move this folder to your pelican plugin directory and enable it in your pelican settings.
2. Add a `<link>` element to the head of your `base.html` to include KaTex' css: `<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.1/dist/katex.min.css" integrity="sha384-dbVIfZGuN1Yq7/1Ocstc1lUEm+AT+/rCkibIcC/OmWo5f0EA48Vf8CytHzGrSwbQ" crossorigin="anonymous">` (or host it yourself, even better ;)).

## Building

To build the javascript module that actually does the latex -> html conversion, go into
the `katex_node_lib` folder and run `yarn run build`, or `npm` respectively.

You should be able to run `node bundle.js` without the `node_modules` folder. This is
used inside `katex_convert.py` to do the conversion.
