const katex = require("katex");
const program = require("commander");

let latexStr = undefined;

program
  .option("-i, --inline", "Render inline (default: display)")
  .option("-q, --quiet", "Don't output stuff to stderr")
  .option("-f, --fullhtml", "Generate entire HTML document (with style and encoding)")
  .arguments("<latexstring>")
  .action(latexstring => {
    latexStr = latexstring;
  })
  .parse(process.argv);

if (!latexStr) {
  console.error("Error: No input latex given! >:(\n");
  program.outputHelp();
  process.exit(1);
}

const html = katex.renderToString(latexStr, {
  throwOnError: true,
  displayMode: !program.inline
});

if (!program.quiet) {
  console.error(`
+-------------------- Generated HTML --------------------
| Don't forget to
| 1. Set encoding of your webpage (<meta charset="utf-8"/>)
| 2. Include the stylesheet for katex
| 3. enjoy :)
+--------------------------------------------------------

`);
}

if(program.fullhtml) {
console.log(`<!DOCTYPE html>
  <html lang="en">
  <head>
  <meta charset="utf-8"/>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.1/dist/katex.min.css" integrity="sha384-dbVIfZGuN1Yq7/1Ocstc1lUEm+AT+/rCkibIcC/OmWo5f0EA48Vf8CytHzGrSwbQ" crossorigin="anonymous">
  </head>
  <body>
  ${html}
  </body>
  </html>
  `);  
} else {
  console.log(html);
}

