"""
Latex-Prerender: Render your equations directly into HTML. No JS at runtime.
KaTex > MathJax, you're welcome.
"""

import re
import markdown
from pelican import signals

from .katex_convert import convert_latex2html


class MarkdownLatexDisplayBlockProcessor(markdown.blockprocessors.BlockProcessor):

    def __init__(self, parser):
        super().__init__(parser)

        self.counter = 1

    def test(self, parent, block):
        return bool(block.startswith("$$"))

    def run(self, parent, blocks):
        latex_str = blocks.pop(0)
        latex_parts = re.match(r"\$\$([\s\S]*?)\$\$", latex_str)
        latex_inner = latex_parts.group(1)

        latex_html = convert_latex2html(latex_inner)

        blocks.insert(0, latex_html)

        self.counter += 1


class MarkdownLatexInlineBlockProcessor(markdown.blockprocessors.BlockProcessor):

    def __init__(self, parser):
        super().__init__(parser)

        self.counter = 1
        self.inline_pattern = r"\$(.+?)\$"

    def test(self, parent, block):
        contains_latex = re.search(self.inline_pattern, block, flags=re.MULTILINE)
        is_block_latex = bool(block.startswith("$$"))

        return not is_block_latex and contains_latex

    def run(self, parent, blocks):
        latex_str = blocks.pop(0)

        regex = re.compile(self.inline_pattern, flags=re.MULTILINE)

        replace_with_latex = lambda mo: convert_latex2html(mo.group(1), display=False)

        latex_html = regex.sub(replace_with_latex, latex_str)

        blocks.insert(0, latex_html)

        self.counter += 1


class MarkdownKatexEtension(markdown.Extension):

    def __init__(self, config):
        super(MarkdownKatexEtension, self).__init__()

    def extendMarkdown(self, md, md_globals):

        display_latex = MarkdownLatexDisplayBlockProcessor(md.parser)
        inline_latex = MarkdownLatexInlineBlockProcessor(md.parser)

        md.parser.blockprocessors.register(display_latex, "latex_prerender_display", 50)
        md.parser.blockprocessors.register(inline_latex, "latex_prerender_inline", 50)


def pelican_init(pelican_obj):
    config = {}
    pelican_obj.settings["MARKDOWN"].setdefault("extensions", []).append(
        MarkdownKatexEtension(config)
    )


def register():
    signals.initialized.connect(pelican_init)
