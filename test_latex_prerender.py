import unittest

from katex_convert import convert_latex2html
from latex_prerender import MarkdownLatexDisplayBlockProcessor
from latex_prerender import MarkdownLatexInlineBlockProcessor


class TestKatexConvert(unittest.TestCase):

    def test_display_latex(self):
        latex = convert_latex2html("\\frac{123}{456}")

        has_display_span = '<span class="katex-display">' in latex
        has_fraction = '<span class="mfrac">' in latex

        return has_display_span and has_fraction

    def test_inline_latex(self):
        latex = convert_latex2html("\\frac{123}{456}", display=False)

        has_display_span = '<span class="katex-display">' in latex
        has_fraction = '<span class="mfrac">' in latex

        return not has_display_span and has_fraction


class MockMd:

    def __init__(self):
        self.tab_length = None


class MockParser:

    def __init__(self):
        self.md = MockMd()


class TestProcessors(unittest.TestCase):

    def setUp(self):
        self.mock_parser = MockParser()
        self.inline_processor = MarkdownLatexInlineBlockProcessor(self.mock_parser)
        self.display_processor = MarkdownLatexInlineBlockProcessor(self.mock_parser)

    ########## Inline Math ##########

    def test_inline_test_block(self):
        return self.inline_processor.test(None, "$$ a = b $$") is False

    def test_inline_test_inline(self):
        return self.inline_processor.test(None, "And therefore $a = b$") is True

    ########## Display Math ##########

    def test_display_test_block(self):
        return self.display_processor.test(None, "$$ a = b $$") is True

    def test_display_test_inline(self):
        return self.display_processor.test(None, "And therefore $a = b$") is False
